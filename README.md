# maniac-releases

## Introduction

Official release definitions for ManIAC are documented in this repository inside of `releases.yml`.

This file contains a YAML dictionary called `releases` with every release of ManIAC.

The official installer for each release uses parts of this file to determine which versions of which components comprise each official release.

## Releases

### Release Codename

The `releases` dictionary contains one dictionary per release codename, using the release's codename as the dictionary name.

```
---
releases:
  release_name_here:
...
```

Releases are named alphabetically after some of our favorite things. :-)

```
---
releases:
  arkham_horror:
...
```

Each release codename dictionary contains two dictionaries. `info` and `components`.

### Info

The `info` dictionary in each release codename dictionary contains metadata about the release.

```
---
releases:
  arkham_horror:
    info:
...
```

#### Number

The `number` variable contains an integer with the numerical release number associated with the release codename.

```
---
releases:
  arkham_horror:
    info:
      number: 1
...
```

#### Released

The `released` variable contains a boolean. If true, the release is stable. If false, the release is a work-in-progress.

```
---
releases:
  arkham_horror:
    info:
      number: 1
      released: false
...
```

#### Installer

The `installer` dictionary within each release's `info` dictionary contains information about where to get the installer.
This includes both the `uri` (used by git, docker, etc) as well as the `version` (which will usually be a tag, branch, commit, etc).

```
---
releases:
  arkham_horror:
    info:
      number: 1
      released: false
      installer:
        uri: git@gitlab.com:dreamer-labs/maniac/maniac_installer.git
        version: v1.0.0
...
```

### Components

The `components` list within the `releases` dictionary contains each item that comprises the release.

```
---
releases:
  arkham_horror:
    info:
      number: 1
      released: false
      installer:
        uri: git@gitlab.com:dreamer-labs/maniac/maniac_installer.git
        version: v1.0.0
    components:
...
```

#### Items

Each item within the `components` list contains the `name`, `uri`, and `version` of each component.

```
---
releases:
  arkham_horror:
    info:
      number: 1
      released: false
      installer:
        uri: git@gitlab.com:dreamer-labs/maniac/maniac_installer.git
        version: "master"
    components:
      - name: "maniac_engine"
        uri: "git@gitlab.com:dreamer-labs/maniac/maniac_engine.git"
        version: "master"
      - name: "zabbix"
        uri: "git@gitlab.com:dreamer-labs/maniac/ansible_role_zabbix.git"
        version: "master"
      - name: "galera"
        uri: "git@gitlab.com:dreamer-labs/maniac/ansible_role_galera.git"
        version: "master"
      - name: "borgbackup"
        uri: "git@gitlab.com:dreamer-labs/maniac/ansible_role_borgbackup.git"
        version: "master"
...
```
